# ArgoCD


## Install 

> Configure connection to the Kube cluster from the terminal


### 1. Create a namespace

```
kubectl create ns argocd
```

### 2. Install the ArgoCD

```
make install
```

### 3. Port-forward

In order to be able to work with ArgoCD GUI, get access to the corresponding service
```
# Within the new terminal window
make port-forward
```

### 4. Get init creds

```
# Get init ArgoCD creds
ARGOCD_PWD=`kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo`
echo && echo admin && echo ${ARGOCD_PWD} && echo
```

### 5. Login to the ArgoCD
```
# Option #1: Web
http://localhost:8080

# Option #2: CLI
argocd login localhost:8080
```


## Clean Up

```
make uninstall
```



