# Demo

This repo contains steps to prepare kubernetes environment, which will be used in order to demonstrate:

- ability of `ArgoCD` to work with multiple value sources;
- ability of `reloader` to update a Pod, if corresponding Secret was updated.




## Prepare Demo Env


### 1. Pre-requisites

1. The `argocd` namespace is created:
```
kubectl create ns argocd
```

### 2. Install ArgoCD

Go to the [1_install_argocd](1_install_argocd) directory and follow the steps from its [README.md](1_install_argocd/README.md) file.


### 3. Install Reloader

Go to the [2_install_reloader](2_install_reloader) directory and follow the steps from its [README.md](2_install_reloader/README.md) file.





## Demo

For the demonstration purposes, the following setup is gonna be used:

![ArgoCD Multiple App Sources POC](./docs/argocdpoc.png)


### Repos

The following public repos are in use:

- [demo-app-cd](https://gitlab.com/xbs-group/demo-app-cd) - contains:
    - the Helm chart, which deploys the app;
    - the *1st value file*, that is used by ArgoCD as a source for the Helm chart (marked as `Helm Values #1` on the diagram);
    - the ArgoCD file, which configures app deployment via its Helm chart.


- [demo-app-external-config](https://gitlab.com/xbs-group/demo-app-external-config) - contains:
    - the *2nd value file*, that is used by ArgoCD as a source for the Helm chart (marked as `Helm Values #2` on the diagram);

- [demo-app-hardcoded-config](https://gitlab.com/xbs-group/demo-app-hardcoded-config) - contains:
    - the *3rd value file*, that is used by ArgoCD as a source for the Helm chart (marked as `Helm Values #3` on the diagram);

- [terraform-demo-app-external-config](https://gitlab.com/xbs-group/terraform-demo-app-external-config) - contains:
    - TF code, which creates *2nd value file* and uploads it to the [demo-app-external-config](https://gitlab.com/xbs-group/demo-app-external-config) repo (so the one file is updated, marked as `Helm Values #2` on the diagram).



## Clean Up


### 1. Clean Up ArgoCD

Go to the [1_install_argocd](1_install_argocd) directory and follow the steps from its [README.md](1_install_argocd/README.md) file.


### 2. Clean Up Reloader

Go to the [2_install_reloader](2_install_reloader) directory and follow the steps from its [README.md](2_install_reloader/README.md) file.


### 3. General Clean Up

1. Remove manually crated namespace
```
kubectl delete ns argocd
```